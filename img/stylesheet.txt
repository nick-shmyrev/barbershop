.sprite {
    background-image: url(spritesheet.png);
    background-repeat: no-repeat;
    display: block;
}

.sprite-checkbox_0 {
    width: 20px;
    height: 20px;
    background-position: -5px -5px;
}

.sprite-checkbox_1 {
    width: 20px;
    height: 20px;
    background-position: -35px -5px;
}

.sprite-facebook_icon {
    width: 12px;
    height: 22px;
    background-position: -65px -5px;
}

.sprite-facebook_icon_black {
    width: 12px;
    height: 22px;
    background-position: -87px -5px;
}

.sprite-instagram_icon {
    width: 21px;
    height: 21px;
    background-position: -5px -37px;
}

.sprite-instagram_icon_black {
    width: 21px;
    height: 21px;
    background-position: -36px -37px;
}

.sprite-login_icon {
    width: 18px;
    height: 16px;
    background-position: -67px -37px;
}

.sprite-modal_close {
    width: 23px;
    height: 23px;
    background-position: -67px -63px;
}

.sprite-modal_login {
    width: 15px;
    height: 15px;
    background-position: -109px -5px;
}

.sprite-modal_psw {
    width: 15px;
    height: 17px;
    background-position: -109px -30px;
}

.sprite-radio_0 {
    width: 20px;
    height: 20px;
    background-position: -100px -57px;
}

.sprite-radio_1 {
    width: 20px;
    height: 20px;
    background-position: -5px -87px;
}

.sprite-vk_icon {
    width: 25px;
    height: 16px;
    background-position: -100px -87px;
}

.sprite-vk_icon_black {
    width: 25px;
    height: 16px;
    background-position: -35px -113px;
}
