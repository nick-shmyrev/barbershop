# Barbershop
HTML Academy HTML&CSS Intensive Course training project

Hand-coded from the .psd file provided by [HTML Academy](https://htmlacademy.ru/)

## See it in action
**[Click.](https://htmlpreview.github.io/?https://github.com/Nick-Shmyrev/22749-barbershop/blob/master/index.html)**
